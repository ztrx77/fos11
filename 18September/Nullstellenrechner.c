#include <stdio.h>
#include <math.h>

int main()
{
    float x,p,q,x1,x2,s;

    printf("Hallo, willkommen zu meinem Nullstellenrechner fuer quadratische Funktionen!\nDie Formel fuer die Rechnung lautet: \nx2 + px + q = 0\n");
    printf("Bitte geben Sie x2 an: ");
    scanf("%f",&x);
    if (x!=1)
    {
        printf("Bitte geben Sie p an: ");
        scanf("%f",&p);
        printf("Bitte geben Sie q an: ");
        scanf("%f",&q);

        p=p/x;
        q=q/x;
        x1=-p/2;
        x2=p/2*p/2-q;
        s= sqrt(x2);
        //printf("p=%.2f,q=%.2f,x1=%.2f,x2=%.2f,s=%.2f",p,q,x1,x2,s);

        if (s>=0)
        { printf("Die Nullstellen der Gleichung sind: x1:%.2f x2:%.2f. L={%.2f,%.2f}",x1+sqrt(x2),x1-sqrt(x2),x1-sqrt(x2),x1+sqrt(x2)); }
        else
        { printf("Da die Diskriminante negativ ist gibt es fuer diese Gleichung keine reellen Loesungen!"); }
    }
    else
    {
        printf("Bitte geben Sie p an: ");
        scanf("%f",&p);
        printf("Bitte geben Sie q an: ");
        scanf("%f",&q);

        x1=-p/2;
        x2=p/2*p/2-q;
        s= sqrt(x2);
        //printf("p=%.2f,q=%.2f,x1=%.2f,x2=%.2f,s=%.2f",p,q,x1,x2,s);

        if (s>=0)
        { printf("Die Nullstellen der Gleichung sind: x1:%.2f x2:%.2f. L={%.2f,%.2f}",x1+sqrt(x2),x1-sqrt(x2),x1-sqrt(x2),x1+sqrt(x2)); }
        else
        { printf("Da die Diskriminante negativ ist gibt es fuer diese Gleichung keine reellen Loesungen!"); }
    }
}