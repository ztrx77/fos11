#include <stdio.h>

int main() {
    float x1,x2,y1,y2,m;
    printf("Punkt A:\nx-Koordinate: ");
    scanf("%f",&x1);
    printf("y-Koordinate: ");
    scanf("%f",&y1);
    printf("Punkt B:\nx-Koordinate: ");
    scanf("%f",&x2);
    printf("y-Koordinate: ");
    scanf("%f",&y2);
    m=(y2-y1)/(x2-x1);
    if(x1==x2 & y1==y2){
        printf("Die Punkte sind identisch!");
    }
    else if(x1==x2 & y1!=y2){
        printf("x=%.2f",x1);
    }
    else if(x1!=x2 & y1==y2){
        printf("y=%.2f",y1-m*x1);
    }
    else if(y1-m*x1==0){
        printf("y=%.2f*x",m);
    }
    else{
        printf("y=%.2f*x+%.2f",m,y1-m*x1);
    }
}