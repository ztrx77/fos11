#include <stdio.h>

int main()

{
    float m1,m2,t1,t2,x;

    printf("Bitte geben Sie die Steigung der ersten Gerade ein: ");
    scanf("%f",&m1);
    printf("Bitte geben Sie den y-Achsrnabschnitt der ersten Gerade ein: ");
    scanf("%f",&t1);
    printf("Bitte geben Sie die Steigung der zweiten Gerade ein: ");
    scanf("%f",&m2);
    printf("Bitte geben Sie den y-Achsrnabschnitt der ersten Gerade ein: ");
    scanf("%f",&t2);

    if (m1==m2 & t1!=t2)
    {
        printf("Die Geraden sind parallel!");
    }
    else if (m1==m2 & t1==t2)
    {
        printf("Die Geraden sind identisch!");
    }
    else
    {
        x=(t2-t1)/(m1-m2);

        printf("S(%.2f|%.2f)",x,m2*x+t2);
    }
}